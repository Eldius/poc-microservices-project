cls

docker-compose stop

docker-compose rm -f

Try
{
    mvn clean package -DskipTests=true

    cd frontend
    grunt
    cd ..

    echo $ERRORLEVEL

    docker-compose build

    docker-compose up

}
Catch
{
    $ErrorMessage = $_.Exception.Message
    $FailedItem = $_.Exception.ItemName

    echo $ErrorMessage
    echo $FailedItem
}
