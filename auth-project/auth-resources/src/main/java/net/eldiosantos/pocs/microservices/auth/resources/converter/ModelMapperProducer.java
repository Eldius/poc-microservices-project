package net.eldiosantos.pocs.microservices.auth.resources.converter;

import net.eldiosantos.pocs.microservices.auth.resources.UserResource;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import net.eldiosantos.pocs.microservices.auth.persistence.model.User;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

/**
 * Created by esjunior on 28/04/2016.
 */
public class ModelMapperProducer {

    private ModelMapper modelMapper;

    @PostConstruct
    public void setup() {
        modelMapper = new ModelMapper();
        modelMapper.addMappings(new PropertyMap<User, UserResource>() {
            @Override
            protected void configure() {
                map().setUsername(source.getUser());
            }
        });
    }

    @Produces
    @ApplicationScoped
    public ModelMapper produce() {
        return modelMapper;
    }
}
