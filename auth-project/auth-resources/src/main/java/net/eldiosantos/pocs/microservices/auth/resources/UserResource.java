package net.eldiosantos.pocs.microservices.auth.resources;

import lombok.Data;

/**
 * Created by esjunior on 01/04/2016.
 */
@Data
public class UserResource {
    private String username;
}
