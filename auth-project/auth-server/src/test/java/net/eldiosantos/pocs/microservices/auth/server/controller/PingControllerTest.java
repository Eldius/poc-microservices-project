package net.eldiosantos.pocs.microservices.auth.server.controller;

import javax.ws.rs.core.Application;

import net.eldiosantos.pocs.microservices.auth.server.response.PingResponse;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PingControllerTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(PingController.class);
    }

    /**
     * Test to see that the message "Got it!" is sent in the response.
     */
    @Test
    public void testGetIt() {
        final PingResponse responseMsg = target().path("ping").request().get(PingResponse.class);

        assertEquals("Hello, Heroku!", responseMsg.getMsg());
    }
}
