package net.eldiosantos.pocs.microservices.auth.server.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import net.eldiosantos.pocs.microservices.auth.persistence.util.transaction.Transactional;
import net.eldiosantos.pocs.microservices.auth.resources.UserResource;
import net.eldiosantos.pocs.microservices.auth.server.resource.CustomUserResource;
import net.eldiosantos.pocs.microservices.auth.service.AddUserService;
import net.eldiosantos.pocs.microservices.auth.service.DeleteUserService;
import net.eldiosantos.pocs.microservices.auth.service.FindUserService;
import net.eldiosantos.pocs.microservices.auth.service.ListUserService;
import net.eldiosantos.pocs.microservices.auth.vo.Credentials;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by esjunior on 05/04/2016.
 */
@Path("user")
@Slf4j
@Api(
    value = "User"
    , description = "Shows application status"
    , tags = {
        "AUTH"
        , "USER"
    }
)
public class UserController {

    @Inject
    private ListUserService listUserService;

    @Inject
    private FindUserService findUserService;

    @Inject
    private AddUserService addUserService;

    @Inject
    private DeleteUserService deleteUserService;

    @Deprecated
    public UserController() {
    }

    public UserController(ListUserService listUserService, FindUserService findUserService, AddUserService addUserService, DeleteUserService deleteUserService) {
        this.listUserService = listUserService;
        this.findUserService = findUserService;
        this.addUserService = addUserService;
        this.deleteUserService = deleteUserService;
    }

    @GET
    @Produces({
        MediaType.APPLICATION_JSON
        , MediaType.APPLICATION_XML
        , MediaType.TEXT_PLAIN
    })
    @ApiOperation(
        value = "List app's users."
        , httpMethod = "GET"
        , notes = "List all the app's users."
        , response = CustomUserResource.class
    )
    public List<CustomUserResource> getAll() {
        return CustomUserResource.build(listUserService.list());
    }

    @GET
    @Path("/{username}")
    @Produces({
        MediaType.APPLICATION_JSON
        , MediaType.APPLICATION_XML
        , MediaType.TEXT_PLAIN
    })
    @ApiOperation(
        value = "Get app's user by username."
        , httpMethod = "GET"
        , notes = "List all the app's users."
        , response = CustomUserResource.class
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "Returned a user", response = CustomUserResource.class)
        , @ApiResponse(code = 404, message = "User not found")
    })
    public Response getSingle(@PathParam("username") final String username) {
        UserResource _user = findUserService.find(username);
        if(_user != null) {
            CustomUserResource user = CustomUserResource.build(_user);
            return Response.status(Response.Status.OK)
                .entity(user)
                .build();
        } else {
            return Response.status(Response.Status.NOT_FOUND)
                .build();
        }
    }

    @POST
    @Produces({
        MediaType.APPLICATION_JSON
    })
    @Consumes({
        MediaType.APPLICATION_JSON
    })
    @Transactional
    @ApiOperation(
        value = "Save new user."
        , httpMethod = "POST"
        , notes = "List all the app's users."
        , response = CustomUserResource.class
    )
    public CustomUserResource post(final Credentials credentials) throws Exception {
        return CustomUserResource.build(addUserService.add(credentials));
    }

    @DELETE
    @Path("/{username}")
    @Transactional
    @ApiOperation(
        value = "Remove user by username."
        , httpMethod = "DELETE"
    )
    public Response delete(@PathParam("username") final String username) {
        deleteUserService.delete(username);
        return Response.status(Response.Status.NO_CONTENT).build();
    }
}
