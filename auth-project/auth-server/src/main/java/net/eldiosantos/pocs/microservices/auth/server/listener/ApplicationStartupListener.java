package net.eldiosantos.pocs.microservices.auth.server.listener;

import lombok.extern.slf4j.Slf4j;
import net.eldiosantos.pocs.microservices.auth.persistence.util.DatabaseCreator;
import net.eldiosantos.pocs.microservices.auth.server.TestdataCreator;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Created by esjunior on 08/04/2016.
 */
@Slf4j
@WebListener("Startup database listener")
public class ApplicationStartupListener implements ServletContextListener {

    @Inject
    private DatabaseCreator creator;

    //@Inject
    //private TestdataCreator testdataCreator;

    @Deprecated
    public ApplicationStartupListener() {
    }

    public ApplicationStartupListener(DatabaseCreator creator) {
        this.creator = creator;
    }


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("Starting auth-server's application context...");
        try {
            creator.create();
            //testdataCreator.createTestData();
        } catch (Exception e) {
            log.error("Error configuring database for auth-server application.", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("Destroying auth-server's application context...");
    }
}
