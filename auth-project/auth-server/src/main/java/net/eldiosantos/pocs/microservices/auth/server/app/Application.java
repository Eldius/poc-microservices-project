package net.eldiosantos.pocs.microservices.auth.server.app;

import io.swagger.jaxrs.config.BeanConfig;
import org.glassfish.jersey.linking.DeclarativeLinkingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.WadlFeature;

/**
 * Created by Eldius on 29/04/2016.
 */
public class Application extends ResourceConfig {
    public Application() {
        packages(
                true
                , "net.eldiosantos.pocs.microservices.auth.server.controller"
                , "io.swagger.resources"
            )
            .register(DeclarativeLinkingFeature.class)
            .register(WadlFeature.class);

        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("0.1");
        beanConfig.setSchemes(new String[]{"http", "https"});
        beanConfig.setHost("192.168.99.100:8080");
        beanConfig.setBasePath("/auth");
        beanConfig.setResourcePackage("net.eldiosantos.pocs.microservices.auth.server.controller");
        beanConfig.setScan(true);
        beanConfig.setDescription("It's the authorization server.");
        beanConfig.setTitle("Auth service");
    }
}
