package net.eldiosantos.pocs.microservices.auth.server.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import net.eldiosantos.pocs.microservices.auth.persistence.util.transaction.Transactional;
import net.eldiosantos.pocs.microservices.auth.server.TestdataCreator;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Created by Eldius on 29/04/2016.
 */
@Path("test")
@Slf4j
public class TestController {

    @Inject
    private TestdataCreator testdataCreator;

    @GET
    @Transactional
    public Response createDate() {
        try {
            testdataCreator.createTestData();
            return Response
                    .status(Response.Status.NO_CONTENT)
                    .build();
        } catch (IllegalStateException e) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .entity(e.getMessage())
                    .build();
        }
    }
}
