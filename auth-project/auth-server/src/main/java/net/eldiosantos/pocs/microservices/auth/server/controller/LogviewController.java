package net.eldiosantos.pocs.microservices.auth.server.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 19/04/2016.
 */
@Path("log")
@Api(
    value = "Log"
    , description = "Shows application logs"
    ,tags = {
        "AUTH"
        , "LOG"
    }
)
public class LogviewController {

    public LogviewController() {
    }

    @GET
    @Produces({
        MediaType.TEXT_PLAIN
    })
    @ApiOperation(
        value = "View app's log file"
        , httpMethod = "GET"
        , response = String.class
    )
    public String get() {
        try (BufferedReader reader = Files.newBufferedReader(Paths.get("applicationLogger.log"))) {
            return reader.lines()
                    .collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
