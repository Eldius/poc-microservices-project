package net.eldiosantos.pocs.microservices.auth.server.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by esjunior on 06/04/2016.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PingResponse {
    private String msg;
}
