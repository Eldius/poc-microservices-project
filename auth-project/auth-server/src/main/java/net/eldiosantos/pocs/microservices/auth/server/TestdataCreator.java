package net.eldiosantos.pocs.microservices.auth.server;

import lombok.extern.slf4j.Slf4j;
import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;
import net.eldiosantos.pocs.microservices.auth.service.AddUserService;
import net.eldiosantos.pocs.microservices.auth.service.CreateUserService;
import net.eldiosantos.pocs.microservices.auth.vo.Credentials;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Arrays;

/**
 * Created by esjunior on 28/04/2016.
 */
@Slf4j
@ApplicationScoped
public class TestdataCreator {

    @Inject
    private UserRepository userRepository;

    @Inject
    private CreateUserService createUserService;

    public TestdataCreator(UserRepository userRepository, CreateUserService createUserService) {
        this.userRepository = userRepository;
        this.createUserService = createUserService;
    }

    @Deprecated
    public TestdataCreator() {
    }

    public void createTestData() {
        final String debugEnv = System.getenv("DEBUG_ENV");
        log.info("Is debug data creation enabled? {}", debugEnv);

        if(Boolean.valueOf(debugEnv)) {
            final String[] userList = {
                    "eldius"
                    , "admin"
                    , "root"
            };

            Arrays.asList(userList)
                .forEach(u -> {
                    try {log.info("Creating user '{}'", u);
                        userRepository.insert(createUserService.create(
                                Credentials.builder()
                                        .user(u)
                                        .pass("strong_pass")
                                        .build()
                        ));
                    } catch (Exception e) {
                        log.error("Error creating a test user", e);
                    }
                });
        } else {
            throw new IllegalStateException("Action not allowed!");
        }
    }
}
