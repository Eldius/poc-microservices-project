package net.eldiosantos.pocs.microservices.auth.server.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.eldiosantos.pocs.microservices.auth.resources.UserResource;
import net.eldiosantos.pocs.microservices.auth.server.controller.UserController;
import org.glassfish.jersey.linking.Binding;
import org.glassfish.jersey.linking.InjectLink;
import org.glassfish.jersey.linking.InjectLinks;

import javax.ws.rs.core.Link;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 05/04/2016.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomUserResource extends UserResource {
    @InjectLink(
            resource=UserController.class
            , rel = "self"
            , method = "getSingle"
            , style = InjectLink.Style.ABSOLUTE
            , bindings = {
            @Binding(name = "username", value = "${instance.username}")
    }
    )
    URI u;

    CustomUserResource(final UserResource user) {
        this.setUsername(user.getUsername());
    }

    public static CustomUserResource build(final UserResource user) {
        return user!=null ? new CustomUserResource(user) : null;
    }

    public static List<CustomUserResource> build(final List<UserResource> users) {
        return users.stream()
                .map(CustomUserResource::build)
                .collect(Collectors.toList());
    }
}
