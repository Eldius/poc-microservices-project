package net.eldiosantos.pocs.microservices.auth.server.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.eldiosantos.pocs.microservices.auth.server.response.PingResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("ping")
@Api(
    value = "Ping"
    , description = "Shows application status"
    , tags = {
        "AUTH"
        , "PING"
    }
)
public class PingController {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces({
            MediaType.TEXT_PLAIN
            , MediaType.APPLICATION_XML
            , MediaType.APPLICATION_JSON
    })
    @ApiOperation(
        value = "Verify auth application's status"
        , httpMethod = "GET"
        , response = PingResponse.class
    )
    public PingResponse getIt() {
        return new PingResponse("Hello, Heroku!");
    }
}
