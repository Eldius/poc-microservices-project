package net.eldiosantos.pocs.microservices.auth.generator;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by esjunior on 11/03/2016.
 */
public class RandomStringGeneratorTest {

    @Test
    public void testGenerate() throws Exception {
        final String string1 = new RandomStringGenerator().generate();
        final String string2 = new RandomStringGenerator().generate();

        assertNotEquals("Generated 2 different strings, right?", string1, string2);
    }

    @Test
    public void testGenerateAgain() throws Exception {
        final RandomStringGenerator generator = new RandomStringGenerator();
        final String string1 = generator.generate();
        final String string2 = generator.generate();

        assertNotEquals("Generated 2 different strings, right?", string1, string2);
    }
}