package net.eldiosantos.pocs.microservices.auth.builder;

import lombok.extern.slf4j.Slf4j;
import net.eldiosantos.pocs.microservices.auth.generator.RandomStringGenerator;
import net.eldiosantos.pocs.microservices.auth.generator.impl.HashGeneratorMockImpl;

import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import org.jglue.cdiunit.ActivatedAlternatives;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.junit.Assert.*;

/**
 * Created by esjunior on 09/03/2016.
 */
@RunWith(CdiRunner.class)
@ActivatedAlternatives({
        HashGeneratorMockImpl.class
})
@AdditionalClasses({
        RandomStringGenerator.class
})
@Slf4j
public class UserBuilderTest {

    @Inject
    private UserBuilder userBuilder;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testPass() throws Exception {
        final User user = userBuilder.start()
                .login("user01")
                .salt("123")
                .password("mypassword")
                .build();

        log.info("generated user:\n" + user.toString());

        assertEquals("User login is ok?", "user01", user.getUser());
        assertEquals("User pass is ok?", "mypassword - 123", user.getPass());
        assertEquals("User salt is ok?", "123", user.getSalt());
    }

    @Test
    public void testHash() throws Exception {
        final User user = userBuilder.start()
                .login("user01")
                .salt("123")
                .passWordHash("_my_password_hash_")
                .build();

        log.info("generated user:\n" + user.toString());

        assertEquals("User login is ok?", "user01", user.getUser());
        assertEquals("User pass is ok?", "_my_password_hash_", user.getPass());
        assertEquals("User salt is ok?", "123", user.getSalt());
    }

    @Test
    public void testGenerateSalt() throws Exception {
        final User user1 = userBuilder.start()
                .login("user01")
                .generateSalt()
                .password("_my_password_hash_")
                .build();
        final User user2 = userBuilder.start()
                .login("user01")
                .generateSalt()
                .password("_my_password_hash_")
                .build();

        assertEquals("Both have the same login...", user1.getUser(), user2.getUser());
        assertNotEquals("They have different passwords, right?", user1.getPass(), user2.getPass());
        assertNotEquals("They have different salts, ok?", user1.getSalt(), user2.getSalt());
    }
}