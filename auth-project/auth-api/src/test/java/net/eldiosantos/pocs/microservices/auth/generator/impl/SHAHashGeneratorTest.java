package net.eldiosantos.pocs.microservices.auth.generator.impl;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by esjunior on 09/03/2016.
 */
public class SHAHashGeneratorTest {

    @Test
    public void testStringHash() throws Exception {
        final String my_strong_pass = new SHAHashGenerator().stringHash("my_strong_pass");
        final String my_strong_pass_ = new SHAHashGenerator().stringHash("my_strong_pass");

        assertEquals("Are the generated hash strings equals?", my_strong_pass, my_strong_pass_);
    }

    @Test
    public void testStringHashAgain() throws Exception {
        final String my_strong_pass = new SHAHashGenerator().stringHash("my_strong_pass");
        final String my_strong_pass_ = new SHAHashGenerator().stringHash("my_strong_pass_");

        assertNotEquals("Are the generated hash strings equals?", my_strong_pass, my_strong_pass_);
    }

    @Test
    public void testStringHash1() throws Exception {
        final String my_strong_pass = new SHAHashGenerator().stringHash("my_strong_pass", "salt");
        final String my_strong_pass_ = new SHAHashGenerator().stringHash("my_strong_pass", "salt");

        assertEquals("Are the salt generated hash strings equals?", my_strong_pass, my_strong_pass_);

    }

    @Test
    public void testStringHash1AgainToo() throws Exception {
        final String my_strong_pass = new SHAHashGenerator().stringHash("my_strong_pass", "salt");
        final String my_strong_pass_ = new SHAHashGenerator().stringHash("my_strong_pass", "salt_");

        assertNotEquals("Are the salt generated hash strings not equals?", my_strong_pass, my_strong_pass_);

    }

    @Test
    public void testStringHashOhGosh() throws Exception {
        final String my_strong_pass = new SHAHashGenerator().stringHash("my_strong_pass", "salt");
        final String my_strong_pass_ = new SHAHashGenerator().stringHash("my_strong_pass_", "salt");

        assertNotEquals("Are the salt generated hash strings not equals?", my_strong_pass, my_strong_pass_);

    }

    @Test
    public void testBinaryHash() throws Exception {
        final byte[] my_strong_passes_hash = new SHAHashGenerator().binaryHash("my_strong_pass");
        final byte[] my_strong_passes_hash_ = new SHAHashGenerator().binaryHash("my_strong_pass");

        assertEquals("Both byte arrays are equals", new String(my_strong_passes_hash), new String(my_strong_passes_hash_));
    }

    @Test
    public void testBinaryHash1() throws Exception {
        final byte[] my_strong_passes_hash = new SHAHashGenerator().binaryHash("my_strong_pass", "salt");
        final byte[] my_strong_passes_hash_ = new SHAHashGenerator().binaryHash("my_strong_pass", "salt");

        assertEquals("Both byte arrays are equals", new String(my_strong_passes_hash), new String(my_strong_passes_hash_));
    }

    @Test
    public void testBinaryHash1Again() throws Exception {
        final byte[] my_strong_passes_hash = new SHAHashGenerator().binaryHash("my_strong_pass", "salt");
        final byte[] my_strong_passes_hash_ = new SHAHashGenerator().binaryHash("my_strong_pass_", "salt");

        assertNotEquals("Both byte arrays are not equals", new String(my_strong_passes_hash), new String(my_strong_passes_hash_));
    }

    @Test
    public void testBinaryHash1OneMoreTime() throws Exception {
        final byte[] my_strong_passes_hash = new SHAHashGenerator().binaryHash("my_strong_pass", "salt_");
        final byte[] my_strong_passes_hash_ = new SHAHashGenerator().binaryHash("my_strong_pass", "salt");

        assertNotEquals("Both byte arrays are not equals", new String(my_strong_passes_hash), new String(my_strong_passes_hash_));
    }

}