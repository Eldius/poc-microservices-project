package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.builder.UserBuilder;
import net.eldiosantos.pocs.microservices.auth.generator.RandomStringGenerator;
import net.eldiosantos.pocs.microservices.auth.generator.impl.HashGeneratorMockImpl;
import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import net.eldiosantos.pocs.microservices.auth.persistence.producer.*;
import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;
import net.eldiosantos.pocs.microservices.auth.persistence.util.DatabaseCreator;
import net.eldiosantos.pocs.microservices.auth.persistence.producer.DatasourceProducer;
import net.eldiosantos.pocs.microservices.auth.vo.Credentials;
import org.apache.ibatis.session.SqlSession;
import org.jglue.cdiunit.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by esjunior on 23/03/2016.
 */
@RunWith(CdiRunner.class)
@ActivatedAlternatives({
        HashGeneratorMockImpl.class
})
@AdditionalClasses({
        RepositoryProducer.class
        , RandomStringGenerator.class
        , SqlSessionFactoryProducer.class
        , SqlSessionProducer.class
        , MyBatisConfigurationProducer.class
        , DatasourceProducer.class
        , UserRepository.class
})
@ProducesAlternative
public class ValidateUserServiceTest {

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserBuilder userBuilder;

    @Inject
    private ValidateUserService validateUserService;

    @Inject
    private SqlSession session;

    @Inject
    private DatabaseCreator creator;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @InRequestScope
    public void testValidate() throws Exception {
        prepareTestdata();

        final Credentials credentials = Credentials.builder()
                .user("case")
                .pass("flatline")
                .build();

        final Boolean isValid = validateUserService.validate(credentials);

        assertTrue("I'm a valid user?", isValid);
    }

    private void prepareTestdata() throws Exception {
        creator.create();
        final User neo = userRepository.find("neo");
        if(neo == null) {
            userRepository.insert(
                    userBuilder.start()
                            .login("neo")
                            .generateSalt()
                            .password("the chosen one")
                            .build()
            );
            userRepository.insert(
                    userBuilder.start()
                            .login("case")
                            .generateSalt()
                            .password("flatline")
                            .build()
            );
            session.commit();
        }
    }

    @Test
    @InRequestScope
    public void testValidateInvalid() throws Exception {
        prepareTestdata();

        final Credentials credentials = Credentials.builder()
                .user("i_forgot_my_pass")
                .pass("neo")
                .build();

        final Boolean isValid = validateUserService.validate(credentials);

        assertFalse("I'm not a valid user... Right?", isValid);
    }

    @Test
    @InRequestScope
    public void testValidateAndGet() throws Exception {
        prepareTestdata();

        final Credentials credentials = Credentials.builder()
                .user("neo")
                .pass("the chosen one")
                .build();

        final Optional<User> neo = validateUserService.validateAndGet(credentials);

        assertTrue("Neo, is that you?", neo.isPresent());
    }

    @Test
    @InRequestScope
    public void testValidateAndGetInvalidCredentials() throws Exception {
        prepareTestdata();

        final Credentials credentials = Credentials.builder()
                .user("case")
                .pass("sin_or_cos_line")
                .build();

        final Optional<User> neo = validateUserService.validateAndGet(credentials);

        assertFalse("You shall not have credentials", neo.isPresent());
    }

    @Test
    @InRequestScope
    public void testValidateAndGetInvalidUser() throws Exception {
        prepareTestdata();

        final Credentials credentials = Credentials.builder()
                .user("molly")
                .pass("my_strong_password!")
                .build();

        final Optional<User> neo = validateUserService.validateAndGet(credentials);

        assertFalse("You are not an user", neo.isPresent());
    }
}
