package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.generator.RandomStringGenerator;
import net.eldiosantos.pocs.microservices.auth.generator.impl.HashGeneratorMockImpl;
import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import net.eldiosantos.pocs.microservices.auth.vo.Credentials;
import org.jglue.cdiunit.ActivatedAlternatives;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.junit.Assert.*;

/**
 * Created by esjunior on 17/03/2016.
 */
@RunWith(CdiRunner.class)
@ActivatedAlternatives({
        HashGeneratorMockImpl.class
})
@AdditionalClasses({
        CreateUserService.class
        , RandomStringGenerator.class
})
public class CreateUserServiceTest {

    @Inject
    private CreateUserService createUserService;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testCreate() throws Exception {
        final User gandalf = createUserService.create(
                Credentials.builder()
                        .user("gandalf")
                        .pass("you shall not pass")
                        .build()
        );

        assertEquals("Is you name Gandalf?", "gandalf", gandalf.getUser());
        assertTrue("Is your pass the same as we defined?", gandalf.getPass().startsWith("you shall not pass"));
    }

    @Test(expected = NullPointerException.class)
    public void testCreateWithoutUser() throws Exception {
        createUserService.create(
                Credentials.builder()
                        .pass("you shall not pass")
                        .build()
        );
    }

    @Test(expected = NullPointerException.class)
    public void testCreateWithoutPass() throws Exception {
        createUserService.create(
                Credentials.builder()
                        .user("gandalf")
                        .build()
        );
    }
}