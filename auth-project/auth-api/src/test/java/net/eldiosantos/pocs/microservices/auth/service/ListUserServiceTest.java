package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.builder.UserBuilder;
import net.eldiosantos.pocs.microservices.auth.generator.RandomStringGenerator;
import net.eldiosantos.pocs.microservices.auth.generator.impl.SHAHashGenerator;
import net.eldiosantos.pocs.microservices.auth.persistence.producer.DatasourceProducer;
import net.eldiosantos.pocs.microservices.auth.persistence.producer.RepositoryProducer;
import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;
import net.eldiosantos.pocs.microservices.auth.persistence.util.DatabaseCreator;
import net.eldiosantos.pocs.microservices.auth.resources.UserResource;
import net.eldiosantos.pocs.microservices.auth.resources.converter.ModelMapperProducer;
import net.eldiosantos.pocs.microservices.auth.vo.Credentials;
import org.apache.ibatis.session.SqlSession;
import org.jglue.cdiunit.AdditionalPackages;
import org.jglue.cdiunit.CdiRunner;
import org.jglue.cdiunit.InRequestScope;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by esjunior on 01/04/2016.
 */
@RunWith(CdiRunner.class)
@AdditionalPackages({
        RepositoryProducer.class
        , ListUserService.class
        , ModelMapperProducer.class
        , CreateUserService.class
        , DatasourceProducer.class
        , SHAHashGenerator.class
        , RandomStringGenerator.class
})
public class ListUserServiceTest {

    @Inject
    private UserBuilder userBuilder;

    @Inject
    private CreateUserService createUserService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private ListUserService listUserService;

    @Inject
    private SqlSession session;

    @Inject
    private DataSource datasource;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @InRequestScope
    public void list() throws Exception {
        prepareTestdata();

        final List<UserResource> list = listUserService.list();

        assertTrue("Is the result grater than zero?", list.size() > 0);
    }

    private void prepareTestdata() throws Exception {
        new DatabaseCreator(datasource).create();

        userRepository.insert(
                createUserService.create(
                        Credentials.builder()
                                .user("ozob")
                                .pass("molotov")
                                .build()
                )
        );
        userRepository.insert(
                createUserService.create(
                        Credentials.builder()
                                .user("bonequicha")
                                .pass("boneca_bicha")
                                .build()
                )
        );

        session.commit();
    }

}
