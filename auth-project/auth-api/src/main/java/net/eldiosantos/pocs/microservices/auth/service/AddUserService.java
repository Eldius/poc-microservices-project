package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;
import net.eldiosantos.pocs.microservices.auth.persistence.util.transaction.Transactional;
import net.eldiosantos.pocs.microservices.auth.resources.UserResource;
import net.eldiosantos.pocs.microservices.auth.vo.Credentials;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 * Created by esjunior on 06/04/2016.
 */
@RequestScoped
public class AddUserService {

    @Inject
    private CreateUserService createUserService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private ModelMapper mapper;

    @Deprecated
    public AddUserService() {
    }

    public AddUserService(CreateUserService createUserService, UserRepository userRepository) {
        this.createUserService = createUserService;
        this.userRepository = userRepository;
    }

    @Transactional
    public UserResource add(final Credentials credentials) throws Exception {
        final User user = createUserService.create(credentials);
        userRepository.insert(user);
        return mapper.map(user, UserResource.class);
    }
}
