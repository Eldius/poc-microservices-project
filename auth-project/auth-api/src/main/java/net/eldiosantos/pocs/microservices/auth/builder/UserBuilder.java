package net.eldiosantos.pocs.microservices.auth.builder;

import net.eldiosantos.pocs.microservices.auth.builder.impl.UserBuilderImpl;
import net.eldiosantos.pocs.microservices.auth.generator.HashGenerator;
import net.eldiosantos.pocs.microservices.auth.generator.RandomStringGenerator;
import net.eldiosantos.pocs.microservices.auth.persistence.model.User;

import javax.inject.Inject;

/**
 * The class used internally to create the user.
 */
public class UserBuilder {
    private final HashGenerator hashGenerator;
    private final RandomStringGenerator stringGenerator;

    @Inject
    public UserBuilder(final HashGenerator hashGenerator, RandomStringGenerator stringGenerator) {
        this.hashGenerator = hashGenerator;
        this.stringGenerator = stringGenerator;
    }

    public LoginBuilder start() {
        return new UserBuilderImpl(hashGenerator, stringGenerator);
    }

    /**
     * Responsible to set the user login.
     */
    public interface LoginBuilder {
        /**
         * Set the user login
         * @param login the user login
         * @return the next step builder
         */
        SaltBuilder login(String login);
    }

    /**
     * Responsible to set or to generate the salt.
     */
    public interface SaltBuilder {
        /**
         * Set an already built salt
         * @param salt the salt itself
         * @return the next step builder
         */
        PasswordBuilder salt(String salt);

        /**
         * Generate the salt
         * @return the next step builder
         */
        PasswordBuilder generateSalt();
    }

    /**
     * Responsible to set the password (and hash it before set)
     * or the alread 'hashed' password.
     */
    public interface PasswordBuilder {
        /**
         * Hash the password before set it as the {@link User}'s
         * password
         * @param pass the password to be hashed
         * @return the next step builder
         * @throws Exception something went wrong during the hash process
         */
        FinalBuilder password(String pass) throws Exception;

        /**
         * Set an already hashed password
         * @param hash the hashed password
         * @return the next step builder
         */
        FinalBuilder passWordHash(String hash);
    }

    /**
     * Build the {@link User}
     * (finally).
     */
    public interface FinalBuilder {
        /**
         * Finally, build the {@link User}.
         * @return the {@link User}
         */
        User build();
    }
}
