package net.eldiosantos.pocs.microservices.auth.generator;

/**
 * The word hasher definition.
 */
public interface HashGenerator {

    /**
     * Hash the phrase and returns a {@link java.lang.String}
     * @param phrase the text to hash
     * @return the phrase hash as {@link java.lang.String}
     * @throws Exception something goes wrong
     */
    String stringHash(String phrase) throws Exception;

    /**
     * Hash the phrase and returns a byte array
     * @param phrase the text to hash
     * @return the phrase hash as a binary array
     * @throws Exception something goes wrong
     */
    byte[] binaryHash(String phrase) throws Exception;

    /**
     * Hash the phrase with a predefined salt and returns a {@link java.lang.String}
     * @param phrase the text to hash
     * @param salt the salt to generate the hash
     * @return the phrase hash as {@link java.lang.String}
     * @throws Exception something goes wrong
     */
    String stringHash(String phrase, String salt) throws Exception;

    /**
     * Hash the phrase with a predefined salt and returns a byte array
     * @param phrase the text to hash
     * @param salt the salt to generate the hash
     * @return the phrase hash as a binary array
     * @throws Exception something goes wrong
     */
    byte[] binaryHash(String phrase, String salt) throws Exception;
}
