package net.eldiosantos.pocs.microservices;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * Created by esjunior on 14/03/2016.
 */
@Slf4j
public class CloseableResourceHandler {

    private final AutoCloseable closeable;

    public CloseableResourceHandler(AutoCloseable closeable) {
        this.closeable = closeable;
    }

    public void execute(CloseableResourceAction action) throws Exception {

        try {
            action.execute(closeable);
        } catch (Exception e) {
            log.error(String.format("Error executing action in a %s", closeable.getClass().getName()), e);
        } finally {
            closeable.close();
        }
    }

    public void execute(List<CloseableResourceAction> actions) throws Exception {
        actions.forEach(a -> {
            try {
                a.execute(closeable);
            } catch (Exception e) {
                log.error(String.format("Error executing action in a %s", closeable.getClass().getName()), e);
            }
        });
        closeable.close();
    }

    public interface CloseableResourceAction {
        void execute(AutoCloseable closeable) throws Exception;
    }
}
