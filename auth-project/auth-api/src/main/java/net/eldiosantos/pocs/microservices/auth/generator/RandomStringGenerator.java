package net.eldiosantos.pocs.microservices.auth.generator;

import java.security.SecureRandom;
import java.util.Base64;

/**
 * A random {@link java.lang.String} generator. Doh.
 */
public class RandomStringGenerator {

    private static final SecureRandom random = new SecureRandom();

    /**
     * Make up the light... Sorry, make up the {@link java.lang.String}.
     * @return The random generated {@link java.lang.String} itself
     */
    public String generate() {

        byte[] bytes = new byte[16];
        random.nextBytes(bytes);
        Base64.Encoder encoder = Base64.getEncoder();

        return encoder.encodeToString(bytes);
    }
}
