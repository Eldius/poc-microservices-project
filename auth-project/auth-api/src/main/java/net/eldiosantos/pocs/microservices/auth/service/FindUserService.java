package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;
import net.eldiosantos.pocs.microservices.auth.resources.UserResource;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 * Created by esjunior on 05/04/2016.
 */
@RequestScoped
public class FindUserService {

    @Inject
    private UserRepository userRepository;

    @Inject
    private ModelMapper mapper;

    @Deprecated
    public FindUserService() {
    }

    public FindUserService(UserRepository userRepository, ModelMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    public UserResource find(final String username) {
        User user = userRepository.find(username);
        return user != null ? mapper.map(user, UserResource.class) : null;
    }
}
