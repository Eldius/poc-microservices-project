package net.eldiosantos.pocs.microservices.auth.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Used to carry the user credentials (username and password)
 * before it's precess.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Credentials {
    /**
     * username
     */
    @NonNull
    private String user;

    /**
     * password
     */
    @NonNull
    private String pass;
}
