package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

/**
 * Created by Eldius on 30/04/2016.
 */
@RequestScoped
public class DeleteUserService {

    @Inject
    private UserRepository repository;

    @Deprecated
    public DeleteUserService() {
    }

    public DeleteUserService(UserRepository repository) {
        this.repository = repository;
    }

    public void delete(final String username) {
        repository.delete(username);
    }
}
