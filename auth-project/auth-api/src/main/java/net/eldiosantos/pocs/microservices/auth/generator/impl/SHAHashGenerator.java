package net.eldiosantos.pocs.microservices.auth.generator.impl;

import net.eldiosantos.pocs.microservices.auth.generator.HashGenerator;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Created by esjunior on 09/03/2016.
 */
public class SHAHashGenerator implements HashGenerator {
    @Override
    public String stringHash(String phrase) throws Exception {
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(binaryHash(phrase));
    }

    @Override
    public byte[] binaryHash(String phrase) throws Exception {
        final MessageDigest hasher = getDigest();
        return hasher.digest(phrase.getBytes("UTF-8"));
    }

    @Override
    public String stringHash(String phrase, String salt) throws Exception {
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(binaryHash(phrase, salt));
    }

    @Override
    public byte[] binaryHash(String phrase, String salt) throws Exception {
        final MessageDigest hasher = getDigest();
        hasher.reset();
        hasher.update(salt.getBytes("UTF-8"));
        return hasher.digest(phrase.getBytes("UTF-8"));
    }

    private MessageDigest getDigest() throws NoSuchAlgorithmException {
        return MessageDigest.getInstance("SHA-256");
    }
}
