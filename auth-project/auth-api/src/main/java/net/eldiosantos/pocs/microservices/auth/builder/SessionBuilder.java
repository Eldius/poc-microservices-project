package net.eldiosantos.pocs.microservices.auth.builder;

import net.eldiosantos.pocs.microservices.auth.generator.RandomStringGenerator;
import net.eldiosantos.pocs.microservices.auth.persistence.model.SessionType;
import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import net.eldiosantos.pocs.microservices.auth.persistence.model.UserSession;
import net.eldiosantos.pocs.microservices.auth.persistence.repository.SessionRepository;

import javax.inject.Inject;
import java.time.Instant;

/**
 * Created by esjunior on 05/05/2016.
 */
public class SessionBuilder {

    @Inject
    private SessionRepository repository;

    @Inject
    private RandomStringGenerator generator;

    public synchronized UserSession build(final User user, final SessionType type) {

        String sessionId;
        do {
            sessionId = generator.generate();
        } while (repository.findSession(sessionId) != null);

        return UserSession.builder()
                .creation(Instant.now())
                .session(sessionId)
                .type(type)
                .userId(user.getUser())
                .build()
                .refresh();
    }

    public synchronized UserSession build(final User user) {
        return this.build(user, SessionType.SHORT_TERM);
    }
}
