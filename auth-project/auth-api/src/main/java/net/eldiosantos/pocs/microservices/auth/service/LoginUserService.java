package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.builder.SessionBuilder;
import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import net.eldiosantos.pocs.microservices.auth.persistence.model.UserSession;
import net.eldiosantos.pocs.microservices.auth.persistence.repository.SessionRepository;
import net.eldiosantos.pocs.microservices.auth.vo.Credentials;

import javax.inject.Inject;
import java.util.Optional;

/**
 * Log in and create a session for the given user.
 */
public class LoginUserService {

    @Inject
    private ValidateUserService validateUserService;

    @Inject
    private SessionBuilder sessionBuilder;

    @Inject
    private SessionRepository sessionRepository;

    /**
     * Create a session for the given user and save it on database.
     * @param credentials user credentials
     * @return the {@link UserSession} fot this user
     * @throws Exception
     */
    public Optional<UserSession> login(final Credentials credentials) throws Exception {
        final Optional<UserSession> result;
        final Optional<User> user = validateUserService.validateAndGet(credentials);
        if(user.isPresent()) {
            final UserSession session = sessionBuilder.build(user.get());
            sessionRepository.insert(session);
            result = Optional.of(session);
        } else {
            result = Optional.empty();
        }

        return result;
    }
}
