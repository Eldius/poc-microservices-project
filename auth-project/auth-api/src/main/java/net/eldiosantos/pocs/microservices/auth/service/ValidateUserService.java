package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.builder.UserBuilder;
import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;
import net.eldiosantos.pocs.microservices.auth.vo.Credentials;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.Optional;

/**
 * It's responsible to validate the user credentials
 * (login and password).
 */
@RequestScoped
public class ValidateUserService {

    @Inject
    private UserBuilder builder;

    @Inject
    private UserRepository userRepository;

    @Deprecated
    public ValidateUserService() {
    }

    public ValidateUserService(UserBuilder builder, UserRepository userRepository) {
        this.builder = builder;
        this.userRepository = userRepository;
    }

    /**
     * Validate the user's credentials
     * @param credentials the user credentials
     * @return true if you passed a valid user credentials
     * @throws Exception when something goes wrong.
     */
    public Boolean validate(final Credentials credentials) throws Exception {
        return validateAndGet(credentials).isPresent();
    }

    /**
     * Validate the user's credentials and return the user if credentials are valid
     * @param credentials the user credentials
     * @return the user identified by the credentials, if it's valid
     * @throws Exception when something goes wrong.
     */
    public Optional<User> validateAndGet(final Credentials credentials) throws Exception {
        final User user = userRepository.find(credentials.getUser());

        try {
            final User user_ = builder.start()
                    .login(credentials.getUser())
                    .salt(user.getSalt())
                    .password(credentials.getPass())
                    .build();

            if(user.getPass().equals(user_.getPass())) {
                return Optional.of(user_);
            } else {
                return Optional.empty();
            }
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }
}
