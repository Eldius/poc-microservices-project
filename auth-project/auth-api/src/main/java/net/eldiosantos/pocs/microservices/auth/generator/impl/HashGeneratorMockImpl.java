package net.eldiosantos.pocs.microservices.auth.generator.impl;

import net.eldiosantos.pocs.microservices.auth.generator.HashGenerator;

import javax.enterprise.inject.Alternative;

/**
 * Created by esjunior on 03/03/2016.
 */
@Alternative
public class HashGeneratorMockImpl implements HashGenerator {
    @Override
    public String stringHash(String phrase) throws Exception {
        return phrase;
    }

    @Override
    public byte[] binaryHash(String phrase) throws Exception {
        return phrase.getBytes("UTF-8");
    }

    @Override
    public String stringHash(String phrase, String salt) throws Exception {
        return phrase + " - " + salt;
    }

    @Override
    public byte[] binaryHash(String phrase, String salt) throws Exception {
        return stringHash(phrase, salt).getBytes("UTF-8");
    }
}
