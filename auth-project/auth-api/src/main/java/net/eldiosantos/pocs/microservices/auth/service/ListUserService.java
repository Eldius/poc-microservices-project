package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;
import net.eldiosantos.pocs.microservices.auth.resources.UserResource;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 01/04/2016.
 */
@RequestScoped
public class ListUserService {

    @Inject
    private UserRepository userRepository;

    @Inject
    private ModelMapper mapper;

    @Deprecated
    public ListUserService() {
    }

    public ListUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserResource>list() {
        return userRepository.list()
                .stream()
                .map((usr) -> mapper.map(usr, UserResource.class))
                .collect(Collectors.toList());
    }
}
