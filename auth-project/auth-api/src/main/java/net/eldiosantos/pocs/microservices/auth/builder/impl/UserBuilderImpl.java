package net.eldiosantos.pocs.microservices.auth.builder.impl;

import net.eldiosantos.pocs.microservices.auth.builder.UserBuilder;
import net.eldiosantos.pocs.microservices.auth.generator.HashGenerator;
import net.eldiosantos.pocs.microservices.auth.generator.RandomStringGenerator;
import net.eldiosantos.pocs.microservices.auth.persistence.model.User;

import javax.inject.Inject;

/**
 * Created by esjunior on 09/03/2016.
 */
public class UserBuilderImpl implements UserBuilder.LoginBuilder, UserBuilder.SaltBuilder, UserBuilder.PasswordBuilder, UserBuilder.FinalBuilder {

    private final HashGenerator hashGenerator;

    private final RandomStringGenerator stringGenerator;

    private final User user;

    @Inject
    public UserBuilderImpl(HashGenerator hashGenerator, RandomStringGenerator stringGenerator) {
        this.hashGenerator = hashGenerator;
        this.stringGenerator = stringGenerator;
        user = new User();
    }

    @Override
    public User build() {
        return user;
    }

    @Override
    public UserBuilder.SaltBuilder login(String login) {
        user.setUser(login);
        return this;
    }

    @Override
    public UserBuilder.FinalBuilder password(String pass) throws Exception {
        user.setPass(hashGenerator.stringHash(pass, user.getSalt()));
        return this;
    }

    @Override
    public UserBuilder.FinalBuilder passWordHash(String hash) {
        user.setPass(hash);
        return this;
    }

    @Override
    public UserBuilder.PasswordBuilder salt(String salt) {
        user.setSalt(salt);
        return this;
    }

    @Override
    public UserBuilder.PasswordBuilder generateSalt() {
        user.setSalt(stringGenerator.generate());
        return this;
    }
}
