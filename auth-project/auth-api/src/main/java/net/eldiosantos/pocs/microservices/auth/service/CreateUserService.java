package net.eldiosantos.pocs.microservices.auth.service;

import net.eldiosantos.pocs.microservices.auth.builder.UserBuilder;
import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import net.eldiosantos.pocs.microservices.auth.vo.Credentials;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * It's responsibility is to create the new user
 * (just create, it doesn't save the created user.
 *
 */
@ApplicationScoped
public class CreateUserService {

    @Inject
    private UserBuilder builder;

    @Deprecated
    public CreateUserService() {
    }

    public CreateUserService(UserBuilder builder) {
        this.builder = builder;
    }

    /**
     * Create the user with the credentials passed as argument
     * @param credentials the {@link User}'s credentials
     * @return The created user
     * @throws Exception when something goes wrong.
     */
    public User create(final Credentials credentials) throws Exception {
        return builder.start()
                .login(credentials.getUser())
                .generateSalt()
                .password(credentials.getPass())
                .build();
    }
}
