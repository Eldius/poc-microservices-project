package net.eldiosantos.pocs.microservices.auth.persistence.repository;

import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import net.eldiosantos.pocs.microservices.auth.persistence.producer.*;
import net.eldiosantos.pocs.microservices.auth.persistence.util.DatabaseCreator;
import net.eldiosantos.pocs.microservices.auth.persistence.producer.DatasourceProducer;
import net.eldiosantos.pocs.microservices.auth.persistence.util.TransactionInterceptor;
import org.apache.ibatis.session.SqlSession;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.jglue.cdiunit.ContextController;
import org.jglue.cdiunit.InRequestScope;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.junit.Assert.*;

/**
 * Created by esjunior on 09/03/2016.
 */
@RunWith(CdiRunner.class)
@AdditionalClasses({
        DatasourceProducer.class
        , MyBatisConfigurationProducer.class
        , SqlSessionFactoryProducer.class
        , TransactionInterceptor.class
        , SqlSessionProducer.class
        , RepositoryProducer.class
        , DatabaseCreator.class
})
public class UserRepositoryTest {

    @Inject
    private UserRepository userRepository;

    @Inject
    private DatabaseCreator databaseCreator;

    @Inject
    private SqlSession session;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    @InRequestScope
    public void testFind() throws Exception {
        databaseCreator.create();
        prepareTestData();

        final User user1 = userRepository.find("user1");
        assertNotNull("Can I see this user?", user1);
        assertEquals("Is the username the same?", "user1", user1.getUser());
    }

    @Test
    @InRequestScope
    public void testList() throws Exception {
        databaseCreator.create();
        prepareTestData();
        assertFalse("There are many users?", userRepository.list().isEmpty());
    }

    private void prepareTestData() {
        try {
            userRepository.insert(new User("user1", "pass1", "salt1"));
            userRepository.insert(new User("user2", "pass2", "salt2"));
            userRepository.insert(new User("user3", "pass3", "salt3"));
            userRepository.insert(new User("user4", "pass4", "salt4"));
            userRepository.insert(new User("user5", "pass5", "salt5"));
            session.commit();
        } catch (Exception e) {

        }
    }
}
