package net.eldiosantos.pocs.microservices.auth.persistence.producer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 * Created by esjunior on 11/03/2016.
 */
public class SqlSessionProducer {

    private final SqlSessionFactory sqlSessionFactory;

    @Inject
    public SqlSessionProducer(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Produces
    @RequestScoped
    public SqlSession produce() {
        return sqlSessionFactory.openSession();
    }
}
