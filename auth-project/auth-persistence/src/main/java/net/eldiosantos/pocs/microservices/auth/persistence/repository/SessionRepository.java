package net.eldiosantos.pocs.microservices.auth.persistence.repository;

import net.eldiosantos.pocs.microservices.auth.persistence.model.UserSession;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * The repository to manipulate the users
 * ({@link UserSession}) class
 * Created by esjunior on 09/03/2016.
 */
public interface SessionRepository {
    /**
     * Find the session by user login
     * @param username User login
     * @return The owner of the username
     */
    @Select("SELECT SESSION , USER_ID , CREATED_AT , VALID_UNTIL , SESSION_TYPE FROM USER_SESSION WHERE USER_ID = #{username}")
    UserSession find(String username);

    /**
     * Insert the {@link UserSession}
     * data on the database
     * @param session a user to save
     */
    @Insert("INSERT INTO USER_SESSION (SESSION, USER_ID, CREATED_AT, VALID_UNTIL, SESSION_TYPE) VALUES (#{session.session}, #{session.userId}, #{session.creation}, #{session.validUntil}, #{session.type}) ")
    void insert(@Param("session") UserSession session);

    /**
     * List all {@link UserSession}s
     * from database
     * @return The {@link UserSession} list
     */
    @Select("SELECT SESSION , USER_ID , CREATED_AT , VALID_UNTIL , SESSION_TYPE FROM USER_SESSION")
    List<UserSession> list();

    @Delete("delete from USER_SESSION where SESSION = #{session.session} ")
    void delete(String session);

    @Delete("SELECT SESSION , USER_ID , CREATED_AT , VALID_UNTIL , SESSION_TYPE FROM USER_SESSION where SESSION = #{session.session} ")
    UserSession findSession(String session);
}
