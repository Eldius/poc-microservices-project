package net.eldiosantos.pocs.microservices.auth.persistence.util;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * Database creator. It will execute all the create table scripts.
 */
@Slf4j
@ApplicationScoped
public class DatabaseCreator {

    @Inject
    private DataSource dataSource;

    @Deprecated
    public DatabaseCreator() {
    }

    public DatabaseCreator(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Create all the things.
     * @throws Exception there was an error during the database creation
     */
    public synchronized void create() throws Exception {

        try {
            if(!verifyDatabase()) {
                final String sql = readScriptFile("create_tables.sql");

                log.info("executing create tables script:\n{}", sql);

                executeSqlScript(sql);

                log.info("Database created.");
            }
        } catch (Exception e) {
            log.info("error creating the database... Maybe it already exists...", e);
        }
    }

    public Boolean verifyDatabase() {
        Boolean result = Boolean.FALSE;
        try {
            final String sql = readScriptFile("test_database.sql");
            final Connection conn = dataSource.getConnection();
            final Statement st = conn.createStatement();
            log.debug("executing create tables script:\n{}", sql);
            try {
                st.executeQuery(sql);
                result = Boolean.TRUE;
            } catch (SQLException e) {
                throw new IllegalStateException("Error trying to verify database state.", e);
            } finally {
                st.close();
                conn.close();
            }
        } catch (Exception e) {
            log.info("Error verifying the database.", e);
            result = Boolean.FALSE;
        }

        return result;
    }

    private String readScriptFile(final String fileName) throws URISyntaxException, IOException {
        log.info("Configuring database connection...");
        final URL fileUrl = ClassLoader.getSystemResource("database/" + fileName);
        final Path path = Paths.get(fileUrl!=null ? fileUrl.toURI() : getClass().getClassLoader().getResource("database/" + fileName).toURI());
        return new String(Files.readAllBytes(path), "utf-8");
    }

    private void executeSqlScript(String script) throws SQLException {
        final Connection conn = dataSource.getConnection();
        final Statement st = conn.createStatement();
        try {
            for(final String sql : script.split(";")) {
                if(!sql.trim().isEmpty()) {
                    st.addBatch(sql.trim());
                }
            }
            st.executeBatch();
        } catch (SQLException e) {
            throw new IllegalStateException("Error trying to verify database state.", e);
        } finally {
            st.close();
            conn.close();
        }
    }
}
