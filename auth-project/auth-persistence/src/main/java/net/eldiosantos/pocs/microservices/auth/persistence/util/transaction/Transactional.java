package net.eldiosantos.pocs.microservices.auth.persistence.util.transaction;

import javax.interceptor.InterceptorBinding;
import static java.lang.annotation.ElementType.*;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by esjunior on 25/04/2016.
 */
@InterceptorBinding
@Target({METHOD, TYPE})
@Retention(RUNTIME)
public @interface Transactional {
}
