package net.eldiosantos.pocs.microservices.auth.persistence.util;

import lombok.extern.slf4j.Slf4j;
import net.eldiosantos.pocs.microservices.auth.persistence.util.transaction.Transactional;
import org.apache.ibatis.session.SqlSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * Created by esjunior on 25/04/2016.
 */
@Transactional
@Interceptor
@Slf4j
public class TransactionInterceptor {

    @Inject
    private SqlSession session;

    @PostConstruct
    public void started() {
        log.debug("Starting a new transactional interceptor...");
    }

    @AroundInvoke
    public Object manageTransaction(InvocationContext ctx) throws Exception {
        Object executionResult;
        try {
            log.debug("Executing a transactional process...");
            executionResult = ctx.proceed();
            log.debug("Comminting transaction...");
            System.out.println("Comminting transaction...");
            session.commit();
            return executionResult;
        } catch (Exception e) {
            session.rollback();
            log.error("Error handling a transaction.", e);
            System.out.println("Error handling a transaction.");
            throw new IllegalStateException("Error handling a transaction", e);
        }
    }
}
