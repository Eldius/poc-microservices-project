package net.eldiosantos.pocs.microservices.auth.persistence.producer;

import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.sql.DataSource;

/**
 * Created by esjunior on 11/03/2016.
 */
public class MyBatisConfigurationProducer {

    private final DataSource dataSource;

    @Inject
    public MyBatisConfigurationProducer(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Produces
    @ApplicationScoped
    public Configuration produce() {
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        addMappers(configuration);
        return configuration;
    }

    private void addMappers(Configuration configuration) {
        configuration.addMapper(UserRepository.class);
    }
}
