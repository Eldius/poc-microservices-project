package net.eldiosantos.pocs.microservices.auth.persistence.producer;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 * Created by esjunior on 11/03/2016.
 */
public class SqlSessionFactoryProducer {

    private final Configuration configuration;

    @Inject
    public SqlSessionFactoryProducer(Configuration configuration) {
        this.configuration = configuration;
    }

    @Produces
    @ApplicationScoped
    public SqlSessionFactory produce() {
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}
