package net.eldiosantos.pocs.microservices.auth.persistence.repository;

import net.eldiosantos.pocs.microservices.auth.persistence.model.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * The repository to manipulate the users
 * ({@link User}) class
 * Created by esjunior on 09/03/2016.
 */
public interface UserRepository {
    /**
     * Find the user by login/username
     * @param username User login
     * @return The owner of the username
     */
    @Select("SELECT user, pass, salt FROM USER_INFO WHERE user = #{username}")
    User find(String username);

    /**
     * Insert the {@link User}
     * data on the database
     * @param user a user to save
     */
    @Insert("insert into USER_INFO (user, pass, salt) values (#{user.user}, #{user.pass}, #{user.salt}) ")
    void insert(@Param("user") User user);

    /**
     * List all {@link User}s
     * from database
     * @return The {@link User} list
     */
    @Select("SELECT user, pass, salt FROM USER_INFO")
    List<User>list();

    @Delete("delete from USER_INFO where user = #{username} ")
    void delete(String username);
}
