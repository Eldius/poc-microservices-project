package net.eldiosantos.pocs.microservices.auth.persistence.producer;

import net.eldiosantos.pocs.microservices.auth.persistence.repository.SessionRepository;
import net.eldiosantos.pocs.microservices.auth.persistence.repository.UserRepository;
import org.apache.ibatis.session.SqlSession;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 * Created by esjunior on 11/03/2016.
 */
public class RepositoryProducer {
    private final SqlSession session;

    @Inject
    public RepositoryProducer(SqlSession session) {
        this.session = session;
    }

    @Produces
    @RequestScoped
    public UserRepository userRepository() {
        return session.getMapper(UserRepository.class);
    }

    @Produces
    @RequestScoped
    public SessionRepository sessionRepository() {
        return session.getMapper(SessionRepository.class);
    }
}
