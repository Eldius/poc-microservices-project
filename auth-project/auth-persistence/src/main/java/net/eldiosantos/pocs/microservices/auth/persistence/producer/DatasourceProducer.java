package net.eldiosantos.pocs.microservices.auth.persistence.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.datasource.pooled.PooledDataSource;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.sql.DataSource;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by esjunior on 11/03/2016.
 */
@Slf4j
public class DatasourceProducer {

    public static final String CONFIG_FILE_NAME = "auth.properties";

    @Produces
    @ApplicationScoped
    public DataSource configure() throws Exception {
        log.info("Configuring database connection...");
        final Properties properties = new Properties();
        final URL propertiesFile = ClassLoader.getSystemResource(CONFIG_FILE_NAME);

        properties.load(
            Files.newBufferedReader(
                Paths.get(
                    propertiesFile!=null?
                        propertiesFile.toURI():
                        getClass().getClassLoader().getResource("auth.properties").toURI()
                )
            )
        );

        final String driver = properties.getProperty("database.driver");
        final String url = properties.getProperty("database.url");
        final String user = properties.getProperty("database.user");
        final String pass = properties.getProperty("database.pass");

        log.info("Database connection configured.");

        return new PooledDataSource(driver, url, user, pass);
    }
}
