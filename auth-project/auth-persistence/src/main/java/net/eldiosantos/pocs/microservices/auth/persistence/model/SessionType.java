package net.eldiosantos.pocs.microservices.auth.persistence.model;

import java.util.concurrent.TimeUnit;

/**
 * Created by esjunior on 29/04/2016.
 */
public enum SessionType {
    LONG_TERM(TimeUnit.DAYS.convert(365, TimeUnit.MILLISECONDS))
    , SHORT_TERM(TimeUnit.MINUTES.convert(15, TimeUnit.MILLISECONDS));

    private final Long validity;

    SessionType(Long validity) {
        this.validity = validity;
    }

    public Long getValidity() {
        return validity;
    }
}
