package net.eldiosantos.pocs.microservices.auth.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

/**
 * Session identification.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserSession {
    private String session;
    private String userId;
    private Instant creation;
    private Instant validUntil;
    private SessionType type;

    public Boolean isStillValid() {
        return validUntil.isBefore(Instant.now());
    }

    public UserSession refresh() {
        this.validUntil.plusMillis(type.getValidity());
        return this;
    }
}
