package net.eldiosantos.pocs.microservices.auth.persistence.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * The user representation.
 */
@Data
@NoArgsConstructor
public class User {

    /**
     * The username
     */
    @NonNull
    private String user;

    /**
     * The password
     */
    @NonNull
    private String pass;

    /**
     * The salt used to hash the password.
     */
    @NonNull
    private String salt;

    public User(String user, String pass, String salt) {
        this.user = user;
        this.pass = pass;
        this.salt = salt;
    }
}
