/**
 * Package to put the database's objects manager producers.
 */
package net.eldiosantos.pocs.microservices.auth.persistence.producer;