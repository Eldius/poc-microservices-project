
FROM jetty

ONBUILD RUN apt-get update && apt-get search graphviz
ONBUILD RUN apt-get update && apt-get install -y graphviz
ONBUILD CP . /tmp/app
ONBUILD RUN mvn site

EXPOSE 8080

COPY ./auth-project/auth-server/target/auth-server.war /usr/local/jetty/webapps

RUN ls -la /usr/local/jetty/webapps &&  java -jar "$JETTY_HOME/start.jar" --add-to-startd=jmx,stats,cdi --create-files --approve-all-licenses

#RUN java -jar "$JETTY_HOME/start.jar" --list-config

#RUN java -jar "$JETTY_HOME/start.jar" --help
