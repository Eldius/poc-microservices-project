package net.eldiosantos.pocs.microservices.largefile;

import java.io.*;

/**
 * Created by esjunior on 04/05/2016.
 */
public class StreamsFileReading extends FileReading {
    @Override
    protected Long readFile(File file) {
        final Long start = System.nanoTime();

        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            reader.lines()
                    .forEach(l->{});
        } catch (Exception e) {
            e.printStackTrace();
        }

        return System.nanoTime() - start;
    }
}
