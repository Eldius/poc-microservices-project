package net.eldiosantos.pocs.microservices.factory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by esjunior on 06/04/2016.
 */
public class FactoryExample {

    private static final Map<Class<?>, Object>map = new HashMap<>();

    static {
        map.put(FirstTestInterface.class, new FirstTestImpl());
        map.put(SecondTestInterface.class, new SecondTestImpl());
        map.put(ThirsdTestImpl.class, new ThirsdTestImpl());
    }

    public <T> T get(Class<T> clazz) {
        final T object = (T) map.get(clazz);
        if(object == null) {
            throw new IllegalArgumentException(String.format("I couldn't find an implementation of %s", clazz.getSimpleName()));
        }
        return object;
    }

    public static interface FirstTestInterface {
        void test();
    }
    public static interface SecondTestInterface {
        void test();
    }
    public static interface ThirsdTestInterface {
        void test();
    }

    public static class FirstTestImpl implements FirstTestInterface {
        public void test() {
            System.out.println(getClass().getName());
        }
    }
    public static class SecondTestImpl implements SecondTestInterface {
        public void test() {
            System.out.println(getClass().getName());
        }
    }
    public static class ThirsdTestImpl implements ThirsdTestInterface {
        public void test() {
            System.out.println(getClass().getName());
        }
    }

    public static void main(String[] params) {
        new FactoryExample().get(FirstTestInterface.class).test();
    }
}
