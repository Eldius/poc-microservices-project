package net.eldiosantos.pocs.microservices.largefile;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;

/**
 * Abstract class to define the file read test procedure
 */
@Slf4j
public abstract class FileReading {

    public static final String BIG_TEXT_FILE_DOWNLAOD_URL = "http://norvig.com/big.txt";
    public static final String BIG_TEXT_FILE_PATH = ".tmp/big.txt";

    protected void verifyFileExists() {
        File localFile = new File(BIG_TEXT_FILE_PATH);
        if(!localFile.exists()) {
            new File(localFile.getParent()).mkdirs();
            final String remoteFilePath = BIG_TEXT_FILE_DOWNLAOD_URL;

            byte[] buffer = new byte[1020];
            try(InputStream in = (InputStream) new URL(remoteFilePath).getContent()) {
                final OutputStream out = new FileOutputStream(localFile);

                while(in.read(buffer) > 0) {
                    out.write(buffer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void readFile() {
        verifyFileExists();

        final Instant start = Instant.now();
        readFile(new File(FileReading.BIG_TEXT_FILE_PATH));
        final Instant finish = Instant.now();



        final StringBuffer msg = new StringBuffer("read file test result:\n")
                .append("class: ")
                .append(getClass().getSimpleName())
                .append("\n")
                .append("elapsed time: \n");
        final Duration duration = Duration.between(start, finish);
        duration.getUnits().forEach(tu -> msg.append(tu.toString()).append(": ").append(tu.getDuration().get(tu)).append("\n"));
        msg.append("nano seconds: ").append(duration.getNano());
        log.info(msg.toString());
        //System.out.println(msg);
    }

    /**
     *
     * @param file file to read
     * @return time to read the entire file in nanoseconds
     */
    protected abstract Long readFile(File file);
}
