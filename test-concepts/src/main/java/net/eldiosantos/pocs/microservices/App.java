package net.eldiosantos.pocs.microservices;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        for(String s:"".split(",")) {
            System.out.println(String.format("valor: %s", s));

        }

        final List list = new ArrayList<>();
        for(String s : "1,2,3,4,5,56".split(",")) {
            try {
                list.add(Long.valueOf(s));
            } catch (NumberFormatException e) {
                list.add(s);
            }
        }
        Serializable test = (Serializable)list;

        System.out.println(test);

    }
}
