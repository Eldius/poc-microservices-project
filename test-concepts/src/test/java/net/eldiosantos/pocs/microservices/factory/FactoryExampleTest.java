package net.eldiosantos.pocs.microservices.factory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by esjunior on 06/04/2016.
 */
public class FactoryExampleTest {

    private final FactoryExample factory = new FactoryExample();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void get() throws Exception {
        assertTrue("It's an instance of FirstTestInterface?", factory.get(FactoryExample.FirstTestInterface.class) instanceof FactoryExample.FirstTestInterface);
    }

    @Test
    public void get1() throws Exception {
        assertTrue("It's an instance of SecondTestInterface?", factory.get(FactoryExample.SecondTestInterface.class) instanceof FactoryExample.SecondTestInterface);
    }

    @Test(expected = IllegalArgumentException.class)
    public void get2() throws Exception {
        factory.get(Number.class);
    }
}
