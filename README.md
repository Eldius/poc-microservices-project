
# POC microservices #
It's a test project to study microservices patterns and architectures.

## Modules ##
 * auth-project
    * auth-api
    * auth-persistence
    * auth-resources
    * auth-server

 * frontend

## Build status ##
 * Drone.io: [![Build Status](https://drone.io/bitbucket.org/Eldius/poc-microservices-project/status.png)](https://drone.io/bitbucket.org/Eldius/poc-microservices-project/latest)

## Projects description ##
  * auth-project:

     A project to provide authorization and autentication to a system using it as an embedded API or as a remote auth
     service (the system make a REST call to validate sessions and to autenticate user credentials).

  * frontend:

     A front end to the whole poc project (all the services added to this project)...
     The main purpose of this project is to create a modular front end who know when a service is available/configured
     and show it's front end just when it's online.

## Download ##
* [Build artifacts](https://drone.io/bitbucket.org/Eldius/poc-microservices-project/files)
  * [auth-api.jar](https://drone.io/bitbucket.org/Eldius/poc-microservices-project/files/auth-project/auth-api/target/auth-api.jar)
  * [auth-api maven site (JAR file)](https://drone.io/bitbucket.org/Eldius/poc-microservices-project/files/auth-project/auth-api/target/auth-api-site.jar)
  * [auth-api Javadocs (JAR file)](https://drone.io/bitbucket.org/Eldius/poc-microservices-project/files/auth-project/auth-api/target/auth-api-javadoc.jar)


## Prepare projects ##
  * auth:

     Just need Java and Maven installed on your computer.

  * frontend:
  
     Install NodeJS (I'm using 5.0.0) and use the command below (if you're using Windows OS you may need to install a
     compiler to install some dependencies).

     ```shell
     npm install -g yo grunt-cli gulp-cli bower generator-angular-fullstack
     ```

