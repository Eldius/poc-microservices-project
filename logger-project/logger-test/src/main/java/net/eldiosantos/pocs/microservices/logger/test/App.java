package net.eldiosantos.pocs.microservices.logger.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 *
 */
public class App {

    private static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main( String[] args ) throws InterruptedException {
        logger.debug("Testing the log implementation...");
        TimeUnit.SECONDS.sleep(10);
        logger.debug("[DEBUG]Testing the log implementation...");
        logger.info("[INFO]Testing the log implementation...");
        logger.warn("[WARN]Testing the log implementation...");
        logger.error("[ERROR]Testing the log implementation...");
        TimeUnit.SECONDS.sleep(30);

        System.exit(0);
    }
}
