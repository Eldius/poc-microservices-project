package org.slf4j.impl;

import lombok.Builder;
import lombok.Data;
import net.eldiosantos.pocs.microservices.logger.service.parser.ExceptionToStacktrace;
import net.eldiosantos.pocs.microservices.logger.service.persistence.MongoLogWriter;
import org.slf4j.Logger;
import org.slf4j.Marker;

import net.eldiosantos.pocs.microservices.logger.service.model.LogEntry;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.List;
import java.util.Set;

/**
 * Created by esjunior on 14/04/2016.
 */
public class MyCustomLoggerAdapter implements Logger {

    private final ExceptionToStacktrace parser = new ExceptionToStacktrace();;
    private final MongoLogWriter logWriter = new MongoLogWriter();;

    @Data
    @Builder
    public static class LogConfig {
        private String name;
        private LogLevel level;
        private Set<String> exclusions;
    }

    private final LogConfig config;

    public MyCustomLoggerAdapter(LogConfig config) {
        this.config = config;
    }

    @Override
    public String getName() {
        return config.getName();
    }

    @Override
    public boolean isTraceEnabled() {
        return config.getLevel().compareTo(LogLevel.TRACE) <= 0;
    }

    @Override
    public void trace(String msg) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(msg, LogLevel.TRACE.toString()).build());
        }
    }

    @Override
    public void trace(String format, Object arg) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.TRACE.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void trace(String format, Object arg1, Object arg2) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.TRACE.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void trace(String format, Object... arguments) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.TRACE.toString(), String.format(format, arguments)).build());
        }
    }

    @Override
    public void trace(String msg, Throwable t) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.TRACE.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
        return isTraceEnabled();
    }

    @Override
    public void trace(Marker marker, String msg) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.TRACE.toString(), msg).build());
        }
    }

    @Override
    public void trace(Marker marker, String format, Object arg) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.TRACE.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void trace(Marker marker, String format, Object arg1, Object arg2) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.TRACE.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void trace(Marker marker, String format, Object... argArray) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.TRACE.toString(), String.format(format, argArray)).build());
        }
    }

    @Override
    public void trace(Marker marker, String msg, Throwable t) {
        if(isTraceEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.TRACE.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    @Override
    public boolean isDebugEnabled() {
        return config.getLevel().compareTo(LogLevel.DEBUG) <= 0;
    }

    @Override
    public void debug(String msg) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), msg).build());
        }
    }

    @Override
    public void debug(String format, Object arg) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void debug(String format, Object arg1, Object arg2) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void debug(String format, Object... arguments) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), String.format(format, arguments)).build());
        }
    }

    @Override
    public void debug(String msg, Throwable t) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
        return isDebugEnabled();
    }

    @Override
    public void debug(Marker marker, String msg) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), msg).build());
        }
    }

    @Override
    public void debug(Marker marker, String format, Object arg) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void debug(Marker marker, String format, Object arg1, Object arg2) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void debug(Marker marker, String format, Object... arguments) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), String.format(format, arguments)).build());
        }
    }

    @Override
    public void debug(Marker marker, String msg, Throwable t) {
        if(isDebugEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.DEBUG.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    @Override
    public boolean isInfoEnabled() {
        return config.getLevel().compareTo(LogLevel.INFO) <= 0;
    }

    @Override
    public void info(String msg) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), msg).build());
        }
    }

    @Override
    public void info(String format, Object arg) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void info(String format, Object arg1, Object arg2) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void info(String format, Object... arguments) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), String.format(format, arguments)).build());
        }
    }

    @Override
    public void info(String msg, Throwable t) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
        return isInfoEnabled();
    }

    @Override
    public void info(Marker marker, String msg) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), msg).build());
        }
    }

    @Override
    public void info(Marker marker, String format, Object arg) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void info(Marker marker, String format, Object arg1, Object arg2) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void info(Marker marker, String format, Object... arguments) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), String.format(format, arguments)).build());
        }
    }

    @Override
    public void info(Marker marker, String msg, Throwable t) {
        if(isInfoEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.INFO.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    @Override
    public boolean isWarnEnabled() {
        return config.getLevel().compareTo(LogLevel.WARN) <= 0;
    }

    @Override
    public void warn(String msg) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), msg).build());
        }
    }

    @Override
    public void warn(String format, Object arg) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void warn(String format, Object... arguments) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), String.format(format, arguments)).build());
        }
    }

    @Override
    public void warn(String format, Object arg1, Object arg2) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void warn(String msg, Throwable t) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    @Override
    public boolean isWarnEnabled(Marker marker) {
        return isWarnEnabled();
    }

    @Override
    public void warn(Marker marker, String msg) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), msg).build());
        }
    }

    @Override
    public void warn(Marker marker, String format, Object arg) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void warn(Marker marker, String format, Object arg1, Object arg2) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void warn(Marker marker, String format, Object... arguments) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), String.format(format, arguments)).build());
        }
    }

    @Override
    public void warn(Marker marker, String msg, Throwable t) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.WARN.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    @Override
    public boolean isErrorEnabled() {
        return config.getLevel().compareTo(LogLevel.ERROR) <= 0;
    }

    @Override
    public void error(String msg) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), msg).build());
        }
    }

    @Override
    public void error(String format, Object arg) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void error(String format, Object arg1, Object arg2) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void error(String format, Object... arguments) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), String.format(format, arguments)).build());
        }
    }

    @Override
    public void error(String msg, Throwable t) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
        return isErrorEnabled();
    }

    @Override
    public void error(Marker marker, String msg) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), msg).build());
        }
    }

    @Override
    public void error(Marker marker, String format, Object arg) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), String.format(format, arg)).build());
        }
    }

    @Override
    public void error(Marker marker, String format, Object arg1, Object arg2) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), String.format(format, arg1, arg2)).build());
        }
    }

    @Override
    public void error(Marker marker, String format, Object... arguments) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), String.format(format, arguments)).build());
        }
    }

    @Override
    public void error(Marker marker, String msg, Throwable t) {
        if(isWarnEnabled()) {
            printLog(getBaseLogBuilder(LogLevel.ERROR.toString(), msg).stackTrace(parser.parse(t)).build());
        }
    }

    public enum LogLevel {
        TRACE, DEBUG, INFO, WARN, ERROR
    }

    private void printLog(final LogEntry log) {
        if(!config.exclusions.stream()
                .filter(exclusion ->log.getName().startsWith(exclusion))
                .findAny()
                .isPresent()
            ) {
            logWriter.persist(log);
        }
    }

    private LogEntry.LogEntryBuilder getBaseLogBuilder(String tag, String msg) {
        return LogEntry.builder()
                .message(msg)
                .thread(Thread.currentThread().getName())
                .name(config.getName())
                .tag(tag)
                .instant(Instant.now());

    }
}
