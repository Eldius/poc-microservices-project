package org.slf4j.impl;

import net.eldiosantos.pocs.microservices.logger.service.utils.PropertiesReader;
import net.eldiosantos.pocs.microservices.logger.slf4j.adapter.ConsoleOutputAdapter;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * Created by esjunior on 14/04/2016.
 */
public class MyCustomLoggerFactory implements ILoggerFactory {

    private static final Map<String, Logger>cache = new HashMap<>();
    private static Properties props;

    static {
        try {
            props = new PropertiesReader().read("logger.properties");
            if(Boolean.valueOf(props.getProperty("logger.console", "false"))) {
                System.setOut(new ConsoleOutputAdapter(new ByteArrayOutputStream(), new MyCustomLoggerAdapter(
                    MyCustomLoggerAdapter.LogConfig
                        .builder()
                        .level(MyCustomLoggerAdapter.LogLevel.valueOf(props.getProperty("logger.level", "INFO")))
                        .exclusions(new HashSet<>(Arrays.asList(props.getProperty("logger.exclusions", "org.mongodb").split(","))))
                        .name("[CONSOL]")
                        .build()
                )));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Logger getLogger(String s) {
        try {
            final Logger logger = cache.getOrDefault(s, new MyCustomLoggerAdapter(
                    MyCustomLoggerAdapter.LogConfig
                            .builder()
                            .level(MyCustomLoggerAdapter.LogLevel.valueOf(props.getProperty("logger.level", "INFO")))
                            .exclusions(new HashSet<>(Arrays.asList(props.getProperty("logger.exclusions", "org.mongodb").split(","))))
                            .name(s)
                            .build()
            ));
            cache.put(s, logger);

            return logger;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
