package net.eldiosantos.pocs.microservices.logger.slf4j;

import org.slf4j.impl.MyCustomLoggerAdapter;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        System.out.println(MyCustomLoggerAdapter.LogLevel.TRACE.compareTo(MyCustomLoggerAdapter.LogLevel.valueOf("DEBUG")));
        System.out.println(MyCustomLoggerAdapter.LogLevel.valueOf("WARN").compareTo(MyCustomLoggerAdapter.LogLevel.DEBUG));
        System.out.println(MyCustomLoggerAdapter.LogLevel.valueOf("WARN").ordinal());
        System.out.println(MyCustomLoggerAdapter.LogLevel.valueOf("TRACE").ordinal());
        System.out.println("|" + MyCustomLoggerAdapter.LogLevel.DEBUG.toString() + "|");
    }
}
