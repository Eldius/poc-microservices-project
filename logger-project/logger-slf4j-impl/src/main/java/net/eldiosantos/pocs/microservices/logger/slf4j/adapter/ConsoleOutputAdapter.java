package net.eldiosantos.pocs.microservices.logger.slf4j.adapter;

import org.slf4j.Logger;

import java.io.*;
import java.util.Locale;

/**
 * Created by esjunior on 18/04/2016.
 */
public class ConsoleOutputAdapter extends PrintStream {

    private final Logger logger;

    public ConsoleOutputAdapter(OutputStream out, Logger logger) {
        super(out);
        this.logger = logger;
    }

    public ConsoleOutputAdapter(OutputStream out, boolean autoFlush, Logger logger) {
        super(out, autoFlush);
        this.logger = logger;
    }

    public ConsoleOutputAdapter(OutputStream out, boolean autoFlush, String encoding, Logger logger) throws UnsupportedEncodingException {
        super(out, autoFlush, encoding);
        this.logger = logger;
    }

    public ConsoleOutputAdapter(String fileName, Logger logger) throws FileNotFoundException {
        super(fileName);
        this.logger = logger;
    }

    public ConsoleOutputAdapter(String fileName, String csn, Logger logger) throws FileNotFoundException, UnsupportedEncodingException {
        super(fileName, csn);
        this.logger = logger;
    }

    public ConsoleOutputAdapter(File file, Logger logger) throws FileNotFoundException {
        super(file);
        this.logger = logger;
    }

    public ConsoleOutputAdapter(File file, String csn, Logger logger) throws FileNotFoundException, UnsupportedEncodingException {
        super(file, csn);
        this.logger = logger;
    }

    @Override
    public void print(boolean b) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + b).toString();
        logger.info(msg);
    }

    @Override
    public void print(char c) {
        final String msg = new StringBuffer("[CONSOLE] ").append(c).toString();
        logger.info(msg);
    }

    @Override
    public void print(int i) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + i).toString();
        logger.info(msg);
    }

    @Override
    public void print(long l) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + l).toString();
        logger.info(msg);
    }

    @Override
    public void print(float f) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + f).toString();
        logger.info(msg);
    }

    @Override
    public void print(double d) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + d).toString();
        logger.info(msg);
    }

    @Override
    public void print(char[] s) {
        final String msg = new StringBuffer("[CONSOLE] ").append(String.valueOf(s)).toString();
        logger.info(msg);
    }

    @Override
    public void print(String s) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + s).toString();
        logger.info(msg);
    }

    @Override
    public void print(Object obj) {
        final String msg = new StringBuffer("[CONSOLE] ").append(obj).toString();
        logger.info(msg);
    }

    @Override
    public void println() {
        final String msg = new StringBuffer("[CONSOLE] ").append("").toString();
        logger.info(msg);
    }

    @Override
    public void println(boolean x) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + x).toString();
        logger.info(msg);
    }

    @Override
    public void println(char x) {
        final String msg = new StringBuffer("[CONSOLE] ").append(x).toString();
        logger.info(msg);
    }

    @Override
    public void println(int x) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + x).toString();
        logger.info(msg);
    }

    @Override
    public void println(long x) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + x).toString();
        logger.info(msg);
    }

    @Override
    public void println(float x) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + x).toString();
        logger.info(msg);
    }

    @Override
    public void println(double x) {
        final String msg = new StringBuffer("[CONSOLE] ").append("" + x).toString();
        logger.info(msg);
    }

    @Override
    public void println(char[] x) {
        final String msg = new StringBuffer("[CONSOLE] ").append(String.valueOf(x)).toString();
        logger.info(msg);
    }

    @Override
    public void println(String x) {
        final String msg = new StringBuffer("[CONSOLE] ").append(x).toString();
        logger.info(msg);
    }

    @Override
    public void println(Object x) {
        final String msg = new StringBuffer("[CONSOLE] ").append(x).toString();
        logger.info(msg);
    }

    @Override
    public PrintStream printf(String format, Object... args) {
        final String msg = new StringBuffer("[CONSOLE] ").append(String.format(format, args)).toString();
        logger.info(msg);
        return this;
    }

    @Override
    public PrintStream printf(Locale l, String format, Object... args) {
        final String msg = new StringBuffer("[CONSOLE] ").append(String.format(l, format, args)).toString();
        logger.info(msg);
        return this;
    }

    @Override
    public PrintStream format(String format, Object... args) {
        final String msg = new StringBuffer("[CONSOLE] ").append(String.format(format, args)).toString();
        logger.info(msg);
        return this;
    }

    @Override
    public PrintStream format(Locale l, String format, Object... args) {
        final String msg = new StringBuffer("[CONSOLE] ").append(String.format(l, format, args)).toString();
        logger.info(msg);
        return this;
    }

    @Override
    public PrintStream append(CharSequence csq) {
        final String msg = new StringBuffer("[CONSOLE] ").append(csq).toString();
        logger.info(msg);
        return this;
    }

    @Override
    public PrintStream append(CharSequence csq, int start, int end) {
        final String msg = new StringBuffer("[CONSOLE] ").append(csq, start, end).toString();
        logger.info(msg);
        return this;
    }

    @Override
    public PrintStream append(char c) {
        final String msg = new StringBuffer("[CONSOLE] ").append(c).toString();
        logger.info(msg);
        return this;
    }

    @Override
    public void write(byte[] b) throws IOException {
        final String msg = new StringBuffer("[CONSOLE] ").append(new String(b, "utf-8")).toString();
        logger.info(msg);
    }
}
