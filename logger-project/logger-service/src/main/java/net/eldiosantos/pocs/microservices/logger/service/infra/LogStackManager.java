package net.eldiosantos.pocs.microservices.logger.service.infra;

import net.eldiosantos.pocs.microservices.logger.service.model.LogEntry;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by esjunior on 14/04/2016.
 */
public class LogStackManager {
    private static final LogStackManager SINGLETON = new LogStackManager();

    private final BlockingQueue<LogEntry> stack = new LinkedBlockingQueue<>();

    private LogStackManager() {

    }

    public static LogStackManager getInstance() {
        return SINGLETON;
    }

    public synchronized LogStackManager push(final LogEntry entry) {
        stack.add(entry);
        return this;
    }

    public synchronized LogEntry pop() {
        return stack.poll();
    }

    public synchronized Optional<LogEntry> popOptional() {
        return stack.isEmpty()? Optional.empty():Optional.of(stack.poll());
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }
}
