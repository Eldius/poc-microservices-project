package net.eldiosantos.pocs.microservices.logger.service.utils;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by esjunior on 14/04/2016.
 */
public class PropertiesReader {

    public Properties read(final String resourceName) throws Exception {
        final Properties properties = new Properties();
        final URL propertiesFile = ClassLoader.getSystemResource(resourceName);

        properties.load(
            Files.newBufferedReader(
                Paths.get(
                    propertiesFile!=null?
                        propertiesFile.toURI():
                        getClass().getClassLoader().getResource(resourceName).toURI()
                )
            )
        );

        return properties;
    }
}
