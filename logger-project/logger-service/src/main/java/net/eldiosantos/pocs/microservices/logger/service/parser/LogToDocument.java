package net.eldiosantos.pocs.microservices.logger.service.parser;

import net.eldiosantos.pocs.microservices.logger.service.model.LogEntry;
import org.bson.Document;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

/**
 * Created by esjunior on 14/04/2016.
 */
public class LogToDocument {
    public Document parse(final LogEntry log) throws Exception {

        return new Document()
            .append("stackTrace", log.getStackTrace())
            .append("instant", log.getDate())
            .append("message", log.getMessage())
            .append("thread", log.getThread())
            .append("tag", log.getTag())
            .append("name", log.getName());
    }
}
