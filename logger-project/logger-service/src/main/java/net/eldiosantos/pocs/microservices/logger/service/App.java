package net.eldiosantos.pocs.microservices.logger.service;

import com.sun.javafx.geom.IllegalPathStateException;
import net.eldiosantos.pocs.microservices.logger.service.model.LogEntry;
import net.eldiosantos.pocs.microservices.logger.service.parser.ExceptionToStacktrace;
import net.eldiosantos.pocs.microservices.logger.service.persistence.MongoLogWriter;

import java.security.SecureRandom;
import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello World!");

        Executors.newSingleThreadExecutor().submit(new LogProducer());

        System.in.read();

        System.out.println("Press a key to stop:");
        System.exit(0);
    }

    private static class LogProducer implements Runnable {

        private final SecureRandom random = new SecureRandom();
        private final MongoLogWriter writer = new MongoLogWriter();

        private static final AtomicInteger counter = new AtomicInteger();

        @Override
        public void run() {
            while (counter.getAndIncrement() < 20) {
                System.out.println(String.format("iteration: %d", counter.get()));
                try {
                    TimeUnit.SECONDS.sleep(3);
                    final int qtd = random.nextInt(5);
                    System.out.println(String.format("generating '%d' log entries", qtd));
                    for(int i = 0; i < qtd; i++) {
                        writer.persist(LogEntry.builder()
                                .stackTrace(getException())
                                .instant(Instant.now())
                                .message("Test message!")
                                .thread(Thread.currentThread().getName())
                                .name("MAIN_LOGGER")
                                .tag("DEBUG")
                                .build());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            System.exit(0);
        }

        private String getException() throws Exception {
            if((Instant.now().getEpochSecond() % 2 > 0)) {
                return new ExceptionToStacktrace().parse(new IllegalPathStateException("Holy crap!"));
            }
            return null;
        }
    }
}
