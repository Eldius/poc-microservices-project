package net.eldiosantos.pocs.microservices.logger.service.persistence;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import net.eldiosantos.pocs.microservices.logger.service.utils.PropertiesReader;

import java.util.Properties;

/**
 * Created by esjunior on 14/04/2016.
 */
public class MongoClientConfiguration {

    private static final String CONFIG_FILE_NAME = "logger.properties";

    private static MongoClient client;

    private static Properties properties;

    public MongoClient getClient() throws Exception {
        if(this.client == null) {
            this.client = makeAClient();
        }
        return this.client;
    }

    private MongoClient makeAClient() throws Exception {
        if(this.properties == null) {
            this.properties = readProperties();
        }
        return new MongoClient(new MongoClientURI(this.properties.getProperty("logger.url")));
    }

    private Properties readProperties() throws Exception {
        return new PropertiesReader().read(CONFIG_FILE_NAME);
    }

    public MongoDatabase getDatabase() throws Exception {
        return getClient().getDatabase(properties.getProperty("logger.database", "logger"));
    }

    public MongoCollection getCollection() throws Exception {
        return getDatabase().getCollection(properties.getProperty("logger.collection", "logger"));
    }
}
