package net.eldiosantos.pocs.microservices.logger.service.persistence;

import net.eldiosantos.pocs.microservices.logger.service.infra.LogStackManager;
import net.eldiosantos.pocs.microservices.logger.service.model.LogEntry;
import net.eldiosantos.pocs.microservices.logger.service.parser.LogToDocument;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by esjunior on 14/04/2016.
 */
public class MongoLogWriter {

    private static final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

    private final LogStackManager stack = LogStackManager.getInstance();

    private static Boolean executorStarted = Boolean.FALSE;

    public MongoLogWriter() {
        if(!executorStarted) {
            System.out.println("Starting the executor...");
            executor.scheduleWithFixedDelay(new PersistenceExecutor(), 0, 1, TimeUnit.SECONDS);
            executorStarted = Boolean.TRUE;
        }
    }

    public void persist(final LogEntry log) {
        System.out.println("Adding a log entry to stack...");
        stack.push(log);
    }

    private class PersistenceExecutor implements Runnable {

        private final MongoClientConfiguration config = new MongoClientConfiguration();
        private final LogToDocument logToDocumentParser = new LogToDocument();

        @Override
        public void run() {
            while(!stack.isEmpty()) {
                try {
                    System.out.println("Inserting a log entry...");
                    config.getCollection().insertOne(logToDocumentParser.parse(stack.pop()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
