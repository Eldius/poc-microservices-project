package net.eldiosantos.pocs.microservices.logger.service.parser;

import net.eldiosantos.pocs.microservices.logger.service.model.LogEntry;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

/**
 * Created by esjunior on 14/04/2016.
 */
public class ExceptionToStacktrace {
    public String parse(final Throwable t) {
        if(t != null) {
            try {
                final ByteArrayOutputStream out = new ByteArrayOutputStream();
                t.printStackTrace(new PrintWriter(out, true));

                return new String(out.toByteArray(), "utf-8");
            } catch (Exception e) {
                throw new IllegalStateException("Error generating stack trace", e);
            }
        } else {
            return null;
        }
    }
}
