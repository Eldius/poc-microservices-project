package net.eldiosantos.pocs.microservices.logger.service.parser;

import net.eldiosantos.pocs.microservices.logger.service.model.LogEntry;
import org.bson.Document;

/**
 * Created by esjunior on 14/04/2016.
 */
public class DocumentToLog {
    public LogEntry parse(final Document doc) {
        return LogEntry.builder()
                .instant(doc.getDate("instant").toInstant())
                .stackTrace(doc.getString("stackTrace"))
                .message(doc.getString("message"))
                .tag(doc.getString("tag"))
                .thread(doc.getString("thread"))
                .build();
    }
}
