package net.eldiosantos.pocs.microservices.logger.service.model;

import lombok.*;

import java.time.Instant;
import java.util.Date;

/**
 * Created by esjunior on 13/04/2016.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogEntry {
    private Instant instant;
    private String message;
    private String stackTrace;
    private String thread;
    private String tag;
    private String name;

    public Date getDate() {
        return new Date(this.getInstant().toEpochMilli());
    }
}
