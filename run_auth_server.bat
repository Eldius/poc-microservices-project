
call mvn clean package -DskipTests=true

set "MAVEN_OPTS=-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n"

Rem call mvn jetty:run -pl auth-project/auth-server

java -jar auth-project\auth-server\target\auth-server.war

set "MAVEN_OPTS="